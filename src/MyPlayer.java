    import java.util.ArrayList;
    import java.util.Random;

    /**
     * University of San Diego
     * COMP 285: Spring 2015
     * Instructor: Gautam Wilkins
     *
     * Implement your Connect Four player class in this file.
     */
    public class MyPlayer extends Player {

        Random rand;
        private int moveNumber;

        private class Move {
            int move;
            double value;

            Move(int move) {
                this.move = move;
                this.value = 0.0;
            }
        }

        public MyPlayer() {
            this.rand = new Random();
            this.moveNumber = 0;
            return;
        }

        public void setPlayerNumber(int number) {
            this.playerNumber = number;
        }


        public int chooseMove(Board gameBoard) {

            for (int i = 0; i < gameBoard.BOARD_SIZE; i++) {
                if (gameBoard.columnXRow(0, 3)) { //if middle column is empty
                    return 3; //move to middle
                }
                else if (gameBoard.columnXRow(1, 3)) { //if space directly above middle column is empty
                    return 3; //place piece here
                }
            }

            long start = System.nanoTime();


            Move bestMove = search(gameBoard, 6, this.playerNumber);
            System.out.println(bestMove.value);


            long diff = System.nanoTime() - start;
            double elapsed = (double) diff / 1e9;
            System.out.println("Elapsed Time: " + elapsed + " sec");

            this.moveNumber++;

            return bestMove.move;


        }


        public Move search(Board gameBoard, int maxDepth, int playerNumber) {

            ArrayList<Move> moves = new ArrayList<Move>();

            // Try each possible move
            for (int i = 0; i < Board.BOARD_SIZE; i++) {

                // Skip this move if the column isn't open
                if (!gameBoard.isColumnOpen(i)) {
                    continue;
                }

                // Place a tile in column i
                Move thisMove = new Move(i);
                gameBoard.move(playerNumber, i);

                // Check to see if that ended the game
                int gameStatus = gameBoard.checkIfGameOver(i);
                if (gameStatus >= 0) {

                    if (gameStatus == 0) {
                        // Tie game
                        thisMove.value = 0.0;
                    } else if (gameStatus == playerNumber) {
                        // Win
                        thisMove.value = 1.0;
                    } else {
                        // Loss
                        thisMove.value = -1.0;
                    }

                } else if (maxDepth == 0) {
                    // If we can't search any more levels down then apply a heuristic to the board
                    thisMove.value = heuristic(gameBoard, playerNumber);

                } else {
                    // Search down an additional level
                    Move responseMove = search(gameBoard, maxDepth - 1, (playerNumber == 1 ? 2 : 1));
                    thisMove.value = -responseMove.value;
                }

                // Store the move
                moves.add(thisMove);

                // Remove the tile from column i
                gameBoard.undoMove(i);
            }

            // Pick the highest value move
            return this.getBestMove(moves);

        }

        public double heuristic(Board gameBoard, int playerNumber) {
            // This should return a number between -1.0 and 1.0.
            //
            // If the board favors playerNumber then the return value should be close to 1.0
            // If the board favors playerNumber's opponent then the return value should be close to 1.0
            // If the board favors neither player then the return value should be close to 0.0
            double lead = 0.0;
            lead+=this.checkHorizontal3(gameBoard, playerNumber);
            lead+=this.checkVertical3(gameBoard, playerNumber);
            lead+=this.checkDiagonal3(gameBoard, playerNumber);
            lead +=this.checkGaps3(gameBoard, playerNumber);

            if(lead == 0) {
                return -1.0; //opposite player  has better chance if winning
            }
            else if(lead > 3) { //you have better chance of winning
                return 1.0;
            }
            else
                return 0.0; //tie
        }




        private Move getBestMove(ArrayList<Move> moves) {

            double max = -1.0;
            Move bestMove = moves.get(0);

            for (Move cm : moves) {
                if (cm.value > max) {
                    max = cm.value;
                    bestMove = cm;
                }
            }

            return bestMove;
        }


        /**
         * Checks for a 3 in a row for the designated player, if they have 3 in a row then they have
         * an advantage
         * @param gameBoard
         * @param playerN
         * @return
         */

        private double checkHorizontal3 (Board gameBoard, int playerN) {
            int adVan = 0;
            //int addP2 = 0;
            //int nada = 0;
            int[][] newGameB = gameBoard.getBoard();
            for(int row = 0; row < 7; row++) {
                for(int column = 0; column < 7; column++){
                    if(newGameB[row][column] == playerN) { //if spot in that row x column has a 0
                        adVan++; //add it for player 1
                    }
                    else {//if(newGameB[row][column] == '0') { //if spot in that row x column has an 'X'
                        adVan = 0; //add it for player 2
                    }
                }
            }
            if (adVan == 3) { //if MyPlayer has a 3 in a row
                return 1.0; //P1 has the advantage
            }
            return 0.0; //P1 doesn't have at least 3 in a row
            }
            //else { //both are equal
                //return 0; //no edge here
            //}

        /**
         * Similar to check Horizontal3, where it checks if there are 3 in a row of MyPlayer's pieces.
         * If so, then MyPlayer has the advantage, if not then returns 0 meaning other player has advantage.
         * @param gameBoard
         * @param playerN
         * @return
         */
        private double checkVertical3 (Board gameBoard, int playerN) {
            int adVan = 0;
            int[][] newGameB = gameBoard.getBoard();
            for(int row = 0; row < 7; row++) {
                for (int column = 0; column < 7; column++) {
                    if (newGameB[column][row] == playerN) { //if that particular column x row has MyPlayer's piece there
                        adVan++; //add it
                    } else
                        adVan = 0; //reset the count if space is blank or has other player's piece
                }
            }
            if (adVan == 3) { //if MyPlayer has 3 in a row
                return 1.0; //then it has the advantage
            }
            return 0.0; //else MyPlayer doesn't have the advantage
        }



        /**
         * Checks to see if there is a 3 in a row vertically, if so it returns a 1.0,
         * if not then assumes other player has a better advantage.
         * @param gameBoard
         * @param playerN
         * @return
         */
    private double checkDiagonal3 (Board gameBoard, int playerN) {
        int adVan = 0;
        int[][] newGameB = gameBoard.getBoard();
        for(int row = 0; row < 4; row++) {
            for(int column = 0; column < 4; column++) {
                if(newGameB[column][row] == playerN) { //if that particular row x column has MyPlayer piece
                    adVan++; //adds it to the count
                    if(newGameB[column+1][row+1] == playerN) { //then checks if the next diagonal space has MyPlayer piece
                        adVan++; //adds it to the count
                        if(newGameB[column+2][row+2] == playerN){ //then checks if following diagonal space has MyPlayer piece
                            adVan++; //adds it to the count
                        }
                    }
                }
            }
        }
        if (adVan == 3) { //if count is 3
            return 1.0; //MyPlayer has advantage
        }
        return 0.0; //count != 3 then MyPlayer doesn't have advantage
    }

        /**
         * 
         * @param gameBoard
         * @param playerN
         * @return
         */
        private double checkGaps3(Board gameBoard, int playerN) {
            int adVan = 0;
            int gapColumn = 0;
            int[][] newGameB = gameBoard.getBoard();
            for(int row = 0; row < 7; row++) {
                for(int column = 0; column < 4; column++) {
                    if(newGameB[row][column] == playerN) { //if that particular row x column has a number related to MyPlayer
                        adVan++; //keep track of it
                        if(newGameB[row][column+1] == 0) { //if space next to it is empty
                            gapColumn++; //add 1 to gap Column
                            if(newGameB[row][column+2] == playerN) { //if following space has MyPlayer number
                                adVan++; //keep note of it
                                if(newGameB[row][column+3] == playerN) { //if space next to it has MyPlayer number
                                    adVan++; //keep track of it
                                }
                            }
                        }
                    }
                }
            }
            if(adVan == 3 && gapColumn == 1) {
                return 1.0; //then MyPlayer has advantage
            }
            return 0.0;
        }

    }






